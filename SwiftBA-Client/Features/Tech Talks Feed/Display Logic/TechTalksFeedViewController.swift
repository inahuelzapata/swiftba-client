//
//  TechTalksFeedViewController.swift
//  SwiftBA-Client
//
//  Created by Nahuel Zapata on 8/10/18.
//  Copyright © 2018 iNahuelZapata. All rights reserved.
//

import Foundation
import UIKit

class TechTalksFeedViewController: UIViewController {
    var techTalks: [TechTalkable] = []
    @IBOutlet var feedTableView: UITableView!
    var provider: TechTalksFeedProvidable = MockTechTalksProvider() //TechTalksFeedProvider()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        fetchTalks()
    }

    func fetchTalks() {
        provider.provide { result in
            do {
                let resolvedResult = try result.resolve()

                self.techTalks = resolvedResult
                self.feedTableView.reloadData()
            } catch {
                // Display error
            }
        }
    }

    func setUpTableView() {
        feedTableView.dataSource = self
        feedTableView.registerNib(TechTalkTableViewCell.self)
    }
}

extension TechTalksFeedViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return techTalks.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TechTalkTableViewCell = tableView.dequeueReusableCell(for: indexPath)

        return cell
    }
}
