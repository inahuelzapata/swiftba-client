//
//  TechTalksEndpoint.swift
//  SwiftBA-Client
//
//  Created by Nahuel Zapata on 8/10/18.
//  Copyright © 2018 iNahuelZapata. All rights reserved.
//

import Foundation
import Leash

enum TechTalksEndpoint: Endpoint {
    case get

    var path: String {
        switch self {
        case .get:
            return "/techTalks"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .get:
            return .get
        }
    }

    var parameters: Any? {
        return [:]
    }
}
