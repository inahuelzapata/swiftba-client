//
//  TechTalksFeedProvider.swift
//  SwiftBA-Client
//
//  Created by Nahuel Zapata on 8/10/18.
//  Copyright © 2018 iNahuelZapata. All rights reserved.
//

import Foundation
import Leash

protocol TechTalksFeedProvidable {
    func provide(completion: @escaping (Result<[TechTalk], Leash.Error>) throws -> Void)
}

class TechTalksFeedProvider: TechTalksFeedProvidable {
    var manager: Manager!
    var client: Client!

    func provide(completion: @escaping (Result<[TechTalk], Leash.Error>) throws -> Void) {
        client.execute(TechTalksEndpoint.get) { (response: Response<[TechTalk]>) in
            guard let talks = response.value else {
                try? completion(.failure(Leash.Error.unknown))
                return
            }

            try? completion(.success(talks))
        }
    }
}
