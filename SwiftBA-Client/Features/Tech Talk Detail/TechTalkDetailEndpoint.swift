//
//  TechTalkDetailEndpoint.swift
//  SwiftBA-Client
//
//  Created by Nahuel Zapata on 8/11/18.
//  Copyright © 2018 iNahuelZapata. All rights reserved.
//

import Foundation
import Leash

enum TechTalkDetailEndpoint: Endpoint {
    case detail(id: Int)

    var path: String {
        switch self {
        case .detail(let id):
            return "/techTalks/\(id)"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .detail:
            return .get
        }
    }

    var parameters: Any? {
        return [:]
    }
}
