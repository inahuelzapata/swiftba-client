//
//  TechTalkDetailViewController.swift
//  SwiftBA-Client
//
//  Created by Nahuel Zapata on 8/10/18.
//  Copyright © 2018 iNahuelZapata. All rights reserved.
//

import Foundation
import UIKit

class TechTalkDetailViewController: UIViewController {
    var techTalk: TechTalk!
    var provider: TechTalkDetailProvidable = TechTalkDetailProvider()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    func fetchTechTalkDetail() {
        provider.provide(byId: techTalk.id) { result in
            do {
                let resolvedResult = try result.resolve()

                self.techTalk = resolvedResult
            } catch {
                // Display error
            }
        }
    }
}
