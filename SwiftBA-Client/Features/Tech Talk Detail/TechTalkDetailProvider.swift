//
//  TechTalkDetailProvider.swift
//  SwiftBA-Client
//
//  Created by Nahuel Zapata on 8/11/18.
//  Copyright © 2018 iNahuelZapata. All rights reserved.
//

import Foundation
import Leash

protocol TechTalkDetailProvidable {
    func provide(byId id: Int, completion: @escaping (Result<TechTalk, Leash.Error>) throws -> Void)
}

class TechTalkDetailProvider: TechTalkDetailProvidable {
    var manager: Manager!
    var client: Client!

    func provide(byId id: Int, completion: @escaping (Result<TechTalk, Leash.Error>) throws -> Void) {
        client.execute(TechTalkDetailEndpoint.detail(id: id)) { (response: Response<TechTalk>) in
            guard let talk = response.value else {
                try? completion(.failure(Leash.Error.unknown))
                return
            }

            try? completion(.success(talk))
        }
    }
}
