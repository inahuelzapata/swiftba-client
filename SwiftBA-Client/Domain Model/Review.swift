//
//  Review.swift
//  SwiftBA-Client
//
//  Created by Nahuel Zapata on 8/10/18.
//  Copyright © 2018 iNahuelZapata. All rights reserved.
//

import Foundation

protocol Reviewable {
    var description: String { get set }
    var stars: Double { get set }
}

struct Review: Codable, Reviewable {
    var description: String
    var stars: Double
}
