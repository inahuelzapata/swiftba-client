//
//  Speaker.swift
//  SwiftBA-Client
//
//  Created by Nahuel Zapata on 8/10/18.
//  Copyright © 2018 iNahuelZapata. All rights reserved.
//

import Foundation

protocol Speakerable {
    var firstName: String { get set }
    var lastName: String { get set }
    var photoUrl: String { get set }
    var githubUrl: String { get set }
}

struct Speaker: Codable, Speakerable {
    var firstName: String
    var lastName: String
    var photoUrl: String
    var githubUrl: String
}
