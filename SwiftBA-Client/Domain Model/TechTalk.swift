//
//  TechTalk.swift
//  SwiftBA-Client
//
//  Created by Nahuel Zapata on 8/10/18.
//  Copyright © 2018 iNahuelZapata. All rights reserved.
//

import Foundation

protocol TechTalkable {
    var title: String { get set }

    var description: String { get set }

    var speaker: Speaker { get set }

    var reviews: [Review] { get set }
}

struct TechTalk: Codable, TechTalkable, Identable {
    var id: Int

    var title: String

    var description: String

    var speaker: Speaker

    var reviews: [Review]
}

protocol Identable {
    var id: Int { get set }
}
